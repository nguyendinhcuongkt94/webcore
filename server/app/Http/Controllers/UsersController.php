<?php

namespace App\Http\Controllers;

use App\Jobs\SendMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="webcore.dev",
 *     basePath="/api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Webcore API documents",
 *         description="This is the api documents for webcore project",
 *         termsOfService="",
 *         @SWG\Contact(name="Duy Nguyen",email="duynb@hiworld.com.vn")
 *     )
 * )
 */

class UsersController extends Controller
{
    private function validation()
    {
        $this->validate($this->request,
            [
                'username' => 'required|username_format',
                'password' => 'required|no_space|min:8|max:16',
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'group' => 'number',
            ]);
    }

    /**
     * @SWG\Get(
     *   path="/users", tags={"user"},
     *   summary="Get list of users",
     *   description="Get list of users",
     *   operationId="index", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="path", description="Auth token", required=true, type="string" ),
     *   @SWG\Response ( response=200, description="Success" )
     * )
     */
    public function index()
    {
        $users = User::getDynamic();
        $this->output(['data' => $users], 200);
    }

    /**
     * @SWG\Get(
     *   path="/users/form", tags={"user"},
     *   summary="Get user by ID",
     *   description="Get user by ID",
     *   operationId="form", produces={"application/json"},
     *   @SWG\Parameter( name="id", in="path", description="ID of user", required=true, type="integer" ),
     *   @SWG\Parameter( name="token", in="path", description="Auth token", required=true, type="string" ),
     *   @SWG\Response ( response=200, description="Success" ),
     *   @SWG\Response ( response=404, description="Not found" )
     * )
     */
    public function getForm()
    {
        $user = User::where([['id', $this->data['id']]])->first();

        if ($user) {
            $this->output(['data' => $user], 200);
        } else {
            $this->output([MESSAGE => trans('User does not exist')], 404);
        }
    }

    /**
     * @SWG\Post(
     *   path="/users/form",
     *   tags={"user"},
     *   summary="Create/Update user information",
     *   description="Create or update user information",
     *   operationId="form", produces={"application/json"},
     *   @SWG\Parameter( name="id", in="formData", description="ID of user (edit)", required=false, type="integer" ),
     *   @SWG\Parameter( name="form_confirm", in="formData", description="If form confirm => just validate only", required=false, type="boolean" ),
     *   @SWG\Parameter( name="token", in="path", description="Auth token", required=true, type="string" ),
     *   @SWG\Parameter( name="username", in="formData", description="User username", required=true, type="string" ),
     *   @SWG\Parameter( name="password", in="formData", description="Password of user", required=true, type="string" ),
     *   @SWG\Parameter( name="name", in="formData", description="Full name of user", required=true, type="string" ),
     *   @SWG\Parameter( name="email", in="formData", description="Email address of user", required=true, type="string" ),
     *   @SWG\Parameter( name="group", in="formData", description="group address of user", required=true, type="integer" ),
     *   @SWG\Parameter( name="avatar", in="formData", description="Avartar of user", required=false, type="string" ),
     *   @SWG\Parameter( name="class", in="formData", description="Class of user", required=false, type="string" ),
     *   @SWG\Parameter( name="gender", in="formData", description="Gender of user", required=false, type="integer" ),
     *   @SWG\Parameter( name="birth_day", in="formData", description="Birth_day of user", required=false, type="string" ),
     *   @SWG\Parameter( name="active", in="formData", description="active of user", required=false, type="integer" ),
     *   @SWG\Response ( response=200, description="Success" ),
     *   @SWG\Response ( response=400, description="Error" )
     * )
     */
    public function saveForm()
    {
        if (isset($this->data['id'])) {
            // Update
            $this->validate($this->request,
                [
                    'username' => 'required|username_format|unique:users,username,' . $this->data['id'],
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $this->data['id'],
                    'group' => 'required',
                    'password' => 'nullable|no_space|min:8|max:16',
                ]);

            if (isset($this->data['password'])) {
                $this->data['password'] = Hash::make($this->data['password']);
            }
        } else {
            // Create new
            $this->validate($this->request,
                [
                    'username' => 'required|username_format|unique:users',
                    'password' => 'required|no_space|min:8|max:16',
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                    'group' => 'required',
                ]);

            $this->data['password'] = Hash::make($this->data['password']);
        }

        $this->saveRecord('User');
    }

    /**
     * @SWG\Post(
     *   path="/users/delete", tags={"user"},
     *   summary="Delete user by ID",
     *   description="Delete user by ID",
     *   operationId="delete", produces={"application/json"},
     *   @SWG\Parameter( name="id", in="formData", description="ID of user", required=true, type="integer" ),
     *   @SWG\Parameter( name="token", in="path", description="Auth token", required=true, type="string" ),
     *   @SWG\Response( response=200, description="Success" ),
     *   @SWG\Response( response=400, description="User does not exist" )
     * )
     */
    public function delete()
    {
        $this->deleteRecord('User');
    }

    /**
     * @SWG\Post(
     *   path="/users/register", tags={"user"},
     *   summary="Register new user",
     *   description="Member register new account from frontend",
     *   operationId="register", produces={"application/json"},
     *   @SWG\Parameter( name="username", in="formData", description="User username", required=true, type="string" ),
     *   @SWG\Parameter( name="password", in="formData", description="Password of user", required=true, type="string" ),
     *   @SWG\Parameter( name="name", in="formData", description="Full name of user", required=true, type="string" ),
     *   @SWG\Parameter( name="email", in="formData", description="Email address of user", required=true, type="string" ),
     *   @SWG\Response( response=200, description="Success" )
     * )
     */
    public function register()
    {
        $this->validation();

        $this->data['password'] = Hash::make($this->data['password']);
        $this->data['active'] = 0;
        $this->data['extra_token'] = mt_rand(1111111111, 9999999999) . time() . mt_rand(1111111111, 9999999999);

        $user = User::create($this->data);
        if ($user) {
            /* ---------- Send activation mail { ---------- */

            $activationLink = URL::to('/api/users/activation/' . $this->data['extra_token']);

            dispatch(new SendMail
                (
                    EMAIL_TEMPLATE_SIGNUP,
                    [
                        'to' => $this->data['email'],
                        'subject' => trans('Member signup confirmation'),
                    ],
                    [
                        'name' => $this->data['name'],
                        'email' => $this->data['email'],
                        'href' => $activationLink,
                    ]
                ));
            /* ---------- Send activation mail } ---------- */

            $this->output([MESSAGE => trans('New user has been registered')], 200);
        }
    }

    public function resetPassword()
    {
        $this->validate($this->request,
            [
                'email' => 'required|email',
            ]);

        $user = User::where('email', $this->data['email'])->first();

        if ($user) {
            if ($user->active == ACTIVE_FALSE) {
                $this->output([MESSAGE => trans('Cannot reset password, let active account first')], 400);
            } else {
                $this->data['extra_token'] = mt_rand(1111111111, 9999999999) . time() . mt_rand(1111111111, 9999999999);
                if ($user->fill($this->data)->save()) {
                    $activationLink = URL::to('/page/forgot-password/' . $this->data['extra_token']);

                    dispatch(new SendMail
                        (
                            EMAIL_TEMPLATE_FORGOT_PASSWORD,
                            [
                                'to' => $this->data['email'],
                                'subject' => trans('Reset password confirmation'),
                            ],
                            [
                                'name' => $user->name,
                                'email' => $this->data['email'],
                                'href' => $activationLink,
                            ]
                        ));
                    /* ---------- Send activation mail } ---------- */
                }
                $this->output([MESSAGE => trans('We have sent link reset password to your email')], 200);
            }
        } else {
            $this->output([MESSAGE => trans('We have sent link reset password to your email')], 200);
        }
    }

    public function resetPasswordActivation()
    {
        $this->validate($this->request,
            [
                'token' => 'required',
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required|min:8',
            ]);

        $token = $this->request['token'];
        $time_token = substr($token, 10, -10);

        $expire_token = $this->getDiffInMinutes($time_token);

        if ($expire_token > 30) {
            $this->output([MESSAGE => trans('Reset password token expired')], 400);
        }

        $user = User::where(['extra_token' => $this->request['token']])->first();
        $new_password = Hash::make($this->data['password']);

        if ($user) {
            $updateData =
                [
                'extra_token' => null,
                'password' => $new_password,
            ];

            $user = User::where(['id' => $user->id])->update($updateData);
            if ($user) {
                $this->output([MESSAGE => trans('Reset password successfully')], 200);
            } else {
                $this->output([MESSAGE => trans('Cannot reset password')], 400);
            }
        } else {
            $this->output([MESSAGE => trans('Invalid activation link')], 400);
        }
    }

    /**
     *  Get the diff in minute
     */
    protected function getDiffInMinutes($time)
    {
        $timetoDiff = Carbon::parse(date('Y/m/d H:i:s', $time));
        $now = Carbon::parse(date('Y/m/d H:i:s', time()));
        return $now->diffInMinutes($timetoDiff);
    }

    /**
     * @SWG\Post(
     *   path="/users/activation", tags={"user"},
     *   summary="Member active account",
     *   description="Member active account from link in mail",
     *   operationId="activation", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="formData", description="The token send to mail", required=true, type="string" ),
     *   @SWG\Response( response=200, description="Success" )
     * )
     */
    public function activation()
    {
        $this->validate($this->request,
            [
                'token' => 'required',
            ]);

        $user = User::where(['extra_token' => $this->data['token']])->first();
        if ($user) {
            $updateData =
                [
                'active' => ACTIVE_TRUE,
                'auth_token' => 'new_auth_token',
                'extra_token' => null,
            ];

            $user = User::where(['id' => $user->id])->update($updateData);
            if ($user) {
                $this->output(['token' => $updateData['auth_token']], 200);
            } else {
                $this->output([MESSAGE => trans('Cannot active account')], 400);
            }
        } else {
            $this->output([MESSAGE => trans('Invalid activation link')], 400);
        }
    }

    /**
     * @SWG\Post(
     *   path="/users/login", tags={"user"},
     *   summary="Member login to the system",
     *   description="Member login to the system",
     *   operationId="login", produces={"application/json"},
     *   @SWG\Parameter( name="username", in="formData", description="Username of user", required=true, type="string" ),
     *   @SWG\Parameter( name="password", in="formData", description="Password of user", required=true, type="string" ),
     *   @SWG\Response
     *   (
     *     response=200,
     *     description="Success",
     *     @SWG\Schema
     *     (
     *       @SWG\Property ( property="token", description="The login token", type="string" ),
     *       @SWG\Property ( property="profile", description="The user profile", type="array" )
     *     )
     *   )
     * )
     */
    public function login()
    {
        $this->validate($this->request,
            [
                'username' => 'required',
                'password' => 'required',
            ]);
        $user = User::where(['username' => $this->data['username']])->first();
        if ($user && Hash::check($this->data['password'], $user->password)) {
            if ($user->active == ACTIVE_TRUE) {
                $data = $user->toArray();
                $data['auth_token'] = sha1('[' . $user->id . '-' . date('Y-m-d H:i:s') . ']');
                User::where(['id' => $user->id])->update($data);
                $this->output(['token' => $data['auth_token'], 'profile' => $user->toArray()], 200);
            } else {
                $this->output(['error-message' => trans('User is blocked')], 422);
            }
        } else {
            $this->output(['error-message' => trans('Invalid username or password')], 422);
        }
    }

    /**
     * @SWG\Get(
     *   path="/users/logout", tags={"user"},
     *   summary="Member login from the system",
     *   description="Member logout from the system",
     *   operationId="logout", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="path", description="The login token", required=true, type="string" ),
     *   @SWG\Response ( response=200, description="Success" ),
     *   @SWG\Response ( response=400, description="Error" )
     * )
     */
    public function logout()
    {
        if ($this->curUser) {
            User::where(['id' => $this->curUser->id])->update(['auth_token' => null]);
            $this->output([MESSAGE => trans('User is logout')], 200);
        } else {
            $this->output([MESSAGE => trans('Invalid request')], 400);
        }
    }

    /**
     * @SWG\Get(
     *   path="/users/check", tags={"user"},
     *   summary="Check the current token is expired or not",
     *   description="heck the current token is expired or not",
     *   operationId="check", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="path", description="The login token", required=true, type="string" ),
     *   @SWG\Response ( response=200, description="Success" ),
     *   @SWG\Response ( response=400, description="Error" )
     * )
     */
    public function checkToken()
    {
        $this->output(['valid' => 1], 200);
    }

    /**
     * @SWG\Post(
     *   path="/users/password", tags={"user"},
     *   summary="Member change his/her password",
     *   description="Member change his/her password",
     *   operationId="password", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="path", description="The login token", required=true, type="string" ),
     *   @SWG\Parameter( name="old_password", in="formData", description="Old password", required=true, type="string" ),
     *   @SWG\Response
     *   (
     *     response=200,
     *     description="Success",
     *     @SWG\Schema
     *     (
     *       @SWG\Property( property="token", description="The login token", type="string" ),
     *       @SWG\Property( property="profile", description="The user profile", type="array" )
     *     )
     *   )
     * )
     */
    public function password()
    {
        $this->validate($this->request,
            [
                'old_password' => 'required|min:8|max:16',
                'new_password' => 'required|min:8|max:16',
                'confirm_password' => 'required|min:8|max:16',
            ]);

        if ($this->data['new_password'] != $this->data['confirm_password']) {
            $this->output(['confirm_password' => [trans('The confirm password does not match')]], 422);die;
        }

        $user = User::where(['id' => $this->curUser->id])->first();
        if ($user && Hash::check($this->data['old_password'], $user->password)) {
            User::where(['id' => $user->id])->update(['password' => Hash::make($this->data['new_password'])]);
            $this->output([MESSAGE => trans('Password has been changed successfully')], 200);
        } else {
            $this->output(['old_password' => [trans('Old password is invalid')]], 422);
        }
    }

    /**
     * @SWG\Get(
     *   path="/users", tags={"user"},
     *   summary="Get current user profile",
     *   description="Get current user profile",
     *   operationId="profile", produces={"application/json"},
     *   @SWG\Parameter( name="token", in="path", description="Auth token", required=true, type="string" ),
     *   @SWG\Response ( response=200, description="Success" )
     * )
     */
    public function getProfile()
    {
        $this->output(['data' => $this->curUser], 200);
    }

    /** Update Profile */
    public function updateProfile()
    {
        $this->validate($this->request,
            [
                'email' => 'required|email|unique:users,email,' . $this->curUser['id'],
                'name' => 'required',
            ]
        );
        $user = User::where(['id' => $this->curUser->id])->first();

        if ($user && $user->fill($this->data)->save()) {
            $this->output([MESSAGE => trans('Profile has been changed successfully')], 200);
        } else {
            $this->output([MESSAGE => trans('Profile cannot be updated')], 400);
        }
    }
}
