<?php

namespace App\Http\Controllers;

class CommonsController extends Controller
{
    public function active()
    {
        $this->validate($this->request, [
            'model' => 'required',
            'id' => 'required',
        ]);

        $model = MODEL_PATH . $this->data['model'];
        $model = $model::where(['id'=>$this->data['id']])->first();
        $model->active = !$model->active;

        if($model->save()) {
            $this->output([MESSAGE=>trans('Active have been switched')],200);
        }
        else {
            $this->output([MESSAGE=>trans('Cannot switch')],400);
        }
    }

    /**
     *  Server code & client code using same env setting
     */
    public function env()
    {
        $env = [
            'BASE_SOCKET' => env('BASE_SOCKET'),
        ];

        $this->output($env, 200);
    }

    /**
     *  Handle the error in angular client & log in to the server
     */
    public function saveError()
    {
        if (!empty($_POST)) {
            $msg = "\n -----------------------------------------";
            foreach ($_POST as $k => $v) {
                $key = htmlentities(strip_tags(urldecode($k)));
                $value = htmlentities(strip_tags(urldecode($v)));
                $msg .= "\n " . $key . ": " . $value;
            }
            $msg .= "\n ----------------------------------------- \n \n ";

            Helpers::saveLog($msg, 'client_errors');
            $this->output(['status' => 200,MESSAGE=>trans('Success')], 200);
        }
        $this->output(['status' => 500,MESSAGE=>trans('Error')], 500);
    }
}