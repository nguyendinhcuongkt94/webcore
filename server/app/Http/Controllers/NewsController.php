<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\News;
use App\Http\Resources\NewsResource;
use Illuminate\Http\Request;
class NewsController extends Controller
{
    public function validation(){
        $this->validate($this->request,
            [
                'title' => 'required|max:100',
                'content' => 'required|no_space|min:20|max:10000',
                'img' => 'image',
            ]);
    }

    public function index()
    {
        $news = News::with('category')->with('user')->getDynamic();
        $this->output(['data' => $news], 200);
    }

    public function get()
    {
        $new = News::where([['id', $this->data['id']]])->with('category')->with('user')->first();
        if ($new) {
            $this->output(['data' => $new], 200);
        } else {
            $this->output([MESSAGE => trans('New does not exist')], 404);
        }
    }

    public function create(Request $request)
    {
        $img = $request->file('img');
        $imgNewName = rand() . '.' . $img->getClientOriginalExtension();
        $img->move('admin/assets/img', $imgNewName);
        $this->data['img'] = $imgNewName;
        if (isset($this->data['id'])) {
            // Update
            $this->validate($this->request,
                [
                    'title' => 'required|max:100',
                    'content' => 'required|min:20|max:10000',
                    // 'img' => 'image',
                ]);
        } else {
            // Create new
            $this->validate($this->request,
                [
                    'title' => 'required|max:100',
                    'content' => 'required|min:20|max:10000',
                    // 'img' => 'image',
                ]);
        }

        $this->saveRecord('News');
    }

    public function delete()
    {
        $this->deleteRecord('News');
    }
}