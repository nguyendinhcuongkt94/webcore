<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    var $curUser; // The current login user
    var $request;
    var $data; // The request data from POST data

    public function __construct(Request $request)
    {
        header('Access-Control-Allow-Origin: *');

        $this->request = $request;
        $this->curUser = $request->curUser;
        //Config::set('app.timezone', 'Asia/Ho_Chi_Minh');

        $this->customDataTransform();
    }

    /**
     *  This function used just to deal with the problem in json format
     */
    private function customDataTransform()
    {
        $data = $this->request->input();

        unset($data['_method']);
        unset($data['token']);

        /* Convert json to array */
        foreach($data as $i => $v)
        {
            if(is_string($v) && is_array(json_decode($v, true)))
            {
                $data[$i] = json_decode($v,true);
            }
        }

        /* Transform null value */
        foreach($data as $key => $value)
        {
            if($value == '')
            {
                $data[$key] = null;
            }
        }

        $this->request->replace($data);
        $this->data = $data;
    }

    /**
     *  The global save function
     */
    public function saveRecord($modelName)
    {
        if (empty($this->data)) {
            $this->output([MESSAGE=>trans('Please enter data before send')], 400);
        }

        $this->formConfirm();
        $model = MODEL_PATH . $modelName;

        if (isset ($this->data['id']))
        {
            $model = $model::where(['id'=>$this->data['id']])->first();

            DB::transaction(function() use ($model)
            {
                try
                {
                    $model->fill($this->data)->save();
                    $this->updateHasMany($model);
                }
                catch (\PDOException $e){throw $e;}
            });
            $this->output([MESSAGE=>trans($modelName.' has been updated')], 200);
        }
        else
        {
            DB::transaction(function() use ($model)
            {
                try
                {
                    $model = $model::create($this->data);
                    $this->updateHasMany($model);
                }
                catch (\PDOException $e){throw $e;}
            });
            $this->output([MESSAGE=>trans($modelName.' has been created')],200);
        }
    }

    /**
     *  This function is used to auto remove/insert new the hasMany items
     */
    function updateHasMany($model)
    {
        if(isset($model->hasMany))
        {
            foreach ($model->hasMany as $strModel)
            {
                if (method_exists($model, $strModel))
                {
                    // Delete old related
                    $model->$strModel()->delete();

                    // Insert new related
                    if (isset($this->data[$strModel])) {
                        $relData = json_decode($this->data[$strModel], true);

                        $foreignKey = $model->$strModel()->getForeignKeyName();
                        $foreignKey = str_replace($strModel . '.', '', $foreignKey);

                        /* --- Set foreign key to the data { --- */
                        $dataWithID = [];
                        $dataWithOutID = [];
                        foreach ($relData as $item) {
                            if (!isset($item[$foreignKey])) {
                                $item[$foreignKey] = $model->id;
                            }

                            if (empty($item['id'])) {
                                array_push($dataWithOutID, $item);
                            } else {
                                array_push($dataWithID, $item);
                            }
                        }
                        /* --- Set foreign key to the data } --- */

                        $model->$strModel()->insert($dataWithID);
                        $model->$strModel()->insert($dataWithOutID);
                        $model->$strModel()->touch();
                    }
                }
            }
        }
    }

    /**
     *  Save multiple records
     */
    public function saveMany($modelName)
    {
        if (empty($this->data)) {
            $this->output([MESSAGE=>trans('Please enter data before send')], 400);
        }

        $this->formConfirm();
        $model = MODEL_PATH . $modelName;
        if (isset ($this->data['id']))
        {
            $record = $model::where(['id'=>$this->data['id']])->first();

            if ($record->fill($this->data)->save())
            {
                $this->output([MESSAGE=>trans($modelName.' has been updated')], 200);
            }
            else{
                $this->output([MESSAGE=>trans($modelName.' cannot be updated')], 400);
            }
        }
        else
        {
            $array_data = $this->data;
            DB::beginTransaction();
            foreach ($array_data as $data) {
                $model::create($data);
            }
            DB::commit();
            $this->output([MESSAGE=>trans($modelName.' has been saved')], 200);
        }
    }

    /**
     *  The global delete function
     */
    public function deleteRecord($modelName)
    {
        $model = MODEL_PATH . $modelName;

        $this->validate($this->request,['id' => 'required']);

        if($record = $model::where([['id', $this->data['id']]])->first())
        {
            DB::transaction(function() use ($record) {
                try
                {
                    $record->delete();
                }
                catch (\PDOException $e)
                {
                    throw $e;
                }
            });
            $this->output([MESSAGE=>trans($modelName.' has been deleted')], 200);
        }
    }

    public function output($data,$statusCode)
    {
        response()->json($data, $statusCode)->send(); die;
    }

    /**
     *  This function run after the validation and skip the save method
     */
    public function formConfirm()
    {
        if(isset($this->data['form_confirm']) && $this->data['form_confirm'] != 0)
        {
            response()->json(['confirm'=>true], 200)->send(); die;
        }
    }
}
