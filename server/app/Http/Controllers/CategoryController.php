<?php

namespace App\Http\Controllers;

use App\Models\Category;

class CategoryController extends Controller
{
    public function validation(){
        $this->validate($this->request,
            [
                'name' => 'required|max:16',
                'description' => 'required|no_space|min:8|max:1000',
            ]);
    }

    public function index()
    {
        $categories = Category::getDynamic();
        $this->output(['data' => $categories], 200);
    }

    public function get()
    {
        $category = Category::where([['id', $this->data['id']]])->first();
        if ($category) {
            $this->output(['data' => $category], 200);
        } else {
            $this->output([MESSAGE => trans('User does not exist')], 404);
        }
    }

    public function create()
    {
        if (isset($this->data['id'])) {
            // Update
            $this->validate($this->request,
                [
                    'name' => 'required|max:16|unique:categories,name,' . $this->data['id'],
                    'description' => 'required|min:8|max:1000',
                ]);
        } else {
            // Create new
            $this->validate($this->request,
                [
                    'name' => 'required|max:16|unique:categories,name',
                    'description' => 'required|min:8|max:1000',
                ]);
        }

        $this->saveRecord('Category');
    }

    public function delete()
    {
        $this->deleteRecord('Category');
    }
}