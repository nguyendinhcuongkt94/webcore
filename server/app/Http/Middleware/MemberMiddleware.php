<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class MemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->curUser)
        {
            if($request->curUser->group == GROUP_MEMBER)
            {
                return $next($request);
            }
            else
            {
                return response([MESSAGE=>trans('Invalid user authorization')], 403);
            }
        }
    }
}
