<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class AppMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->curUser = null;
        if($request->input('token'))
        {
            $request->curUser = User::where('auth_token', $request->input('token'))->first();
        }

        return $next($request);
    }
}
