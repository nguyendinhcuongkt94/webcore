<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the validation services for the application.
     *
     * @return void
     */
    public function boot()
    {
        /* Validation username format */
        Validator::extend('username_format', function($attr, $value, $parameters)
        {
            return preg_match('/^[a-zA-Z0-9]+$/', $value);
        }, trans('Invalid username format.'));

        /* Validation password format:
           - English uppercase characters (A – Z)
           - English lowercase characters (a – z)
           - Base 10 digits (0 – 9)
           - Non-alphanumeric (For example: !, $, #, or %)
           - Unicode characters
        */
        Validator::extend('password_complex', function($attr, $value, $parameters)
        {
            return preg_match('/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/', $value);
        }, trans('Invalid password format.'));

        /* Validation price format */
        Validator::extend('price_format', function($attr, $value, $parameters)
        {
            return preg_match('/^\d*(\.\d{1,2})?$/', $value);
        }, trans('Invalid price format.'));

        /* Validation no space */
        Validator::extend('no_space', function($attr, $value, $parameters)
        {
            return preg_match('/^\S*$/u', $value);
        }, trans('Field must not have spaces.'));

    }
}
