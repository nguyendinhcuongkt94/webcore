<?php
namespace App\Models;

class Category extends AppModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "categories";

    protected $fillable = [
        'id', 'name', 'description',
    ];
    public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

}
