<?php
namespace App\Models;

class News extends AppModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "news";
    
    protected $fillable = [
        'id','title','img','content','category_id','user_id','created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
     {
         return $this->belongsTo('App\Models\User', 'user_id', 'id')->select(array('id', 'email','name','username'));;
     }

     public function category()
     {
         return $this->belongsTo('App\Models\Category', 'category_id', 'id')->select(array('id', 'name'));;
     }
}