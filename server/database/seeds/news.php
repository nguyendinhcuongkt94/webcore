<?php

use Illuminate\Database\Seeder;

class news extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 100;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('news')->insert([
                'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'img' => $faker->randomElement(['item-1.jpg','item-2.jpg','item-3.jpg','item-4.jpg','item-5.jpg','item-6.jpg','item-7.jpg','item-8.jpg','item-9.jpg','item-10.jpg','item-11.jpg','item-12.jpg',]),
                'content' => $faker->text($maxNbChars = 10000),
                'category_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7]),
                'user_id' => $faker->randomElement([1]),
            ]);
        }
    }
}
