<?php

$router->group(['prefix' => API_PREFIX], function() use ($router)
{
    $router->get('/', function (){return 'This is api page.';});

    /** Commons Route */
    $router->group(['prefix' => 'commons'], function() use ($router)
    {
        $router->get('/env', ['uses' => 'CommonsController@env']);
        $router->post('/saveError', ['uses' => 'CommonsController@saveError']);

        $router->group(['middleware' => 'auth'], function () use ($router)
        {
            $router->post('/active', ['uses' => 'CommonsController@active']);
        });
    });

});

