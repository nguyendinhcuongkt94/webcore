<?php

$router->group(['prefix' => API_PREFIX], function() use ($router)
{
    $router->get('categories', ['uses' => 'CategoryController@index']);

    /* ---------- Auth api category here { ---------- */
    $router->group(['middleware' => 'auth'], function () use ($router)
    {

        $router->get('categories/form', ['uses' => 'CategoryController@get']);
        $router->post('categories/form', ['uses' => 'CategoryController@create']);  
        $router->post('categories/delete', ['uses' => 'CategoryController@delete']);                              
        // Api for admin only
        $router->group(['middleware' => 'admin'], function () use ($router)
        {
            // $router->get('categories', ['uses' => 'CategoryController@index']);
        });

        //Api for member only
        $router->group(['middleware' => 'member'], function () use ($router)
        {

        });
    });
    /* ---------- Auth api here } ---------- */
});


