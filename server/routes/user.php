<?php

$router->group(['prefix' => API_PREFIX], function() use ($router)
{
    /* ---------- Public api here { ---------- */
    $router->post('users/login', ['uses' => 'UsersController@login']);
    $router->post('users/register', ['uses' => 'UsersController@register']);
    $router->post('users/resetPassword', ['uses' => 'UsersController@resetPassword']);
    $router->post('users/resetPassword-activation', ['uses' => 'UsersController@resetPasswordActivation']);
    /* ---------- Public api here } ---------- */

    /* ---------- Auth api here { ---------- */
    $router->group(['middleware' => 'auth'], function () use ($router)
    {

        $router->get('users/check', ['uses' => 'UsersController@checkToken']);
        $router->get('users/form', ['uses' => 'UsersController@getForm']);
        $router->post('users/form', ['uses' => 'UsersController@saveForm']);
        $router->post('users/delete', ['uses' => 'UsersController@delete']);
        $router->get('users/profile', ['uses' => 'UsersController@getProfile']);
        $router->post('users/profile', ['uses' => 'UsersController@updateProfile']);
        $router->get('users/logout', ['uses' => 'UsersController@logout']);
        $router->post('users/activation', ['uses' => 'UsersController@activation']);
        $router->post('users/password', ['uses' => 'UsersController@password']);
        $router->post('users/updateprofile', ['uses' => 'UsersController@updateProfile']);

        // Api for admin only
        $router->group(['middleware' => 'admin'], function () use ($router)
        {
            $router->get('users/', ['uses' => 'UsersController@index']);
        });

        // Api for member only
        $router->group(['middleware' => 'member'], function () use ($router)
        {

        });
    });
    /* ---------- Auth api here } ---------- */
});
