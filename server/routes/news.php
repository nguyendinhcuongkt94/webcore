<?php

$router->group(['prefix' => API_PREFIX], function() use ($router)
{
    $router->get('news', ['uses' => 'NewsController@index']);

    /* ---------- Auth api category here { ---------- */
    $router->group(['middleware' => 'auth'], function () use ($router)
    {

        $router->get('news/form', ['uses' => 'NewsController@get']);
        $router->post('news/form', ['uses' => 'NewsController@create']);
        $router->post('news/delete', ['uses' => 'NewsController@delete']);                              
        
        // Api for admin only
        $router->group(['middleware' => 'admin'], function () use ($router)
        {
            // $router->get('news', ['uses' => 'NewsController@index']);
        });

        //Api for member only
        $router->group(['middleware' => 'member'], function () use ($router)
        {

        });
    });
    /* ---------- Auth api here } ---------- */
});


