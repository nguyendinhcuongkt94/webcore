<?php

/* --- System Group --- */
if (!defined('GROUP_ADMIN')) { define('GROUP_ADMIN', 1); } // Default
if (!defined('GROUP_MEMBER')) { define('GROUP_MEMBER', 2); }

return [
    'User'=> [
        'gender'=>[0=>trans('Female'),1=>trans('Male')],
        'group'=> [
            GROUP_ADMIN => trans('Group admin'),
            GROUP_MEMBER => trans('Group member'),
        ]
    ]
];
?>