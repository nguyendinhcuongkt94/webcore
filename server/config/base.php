<?php

/* --- Basic constant --- */
if (!defined('MESSAGE')) {define('MESSAGE', 'message');}
if (!defined('MODEL_PATH')) {define('MODEL_PATH', '\App\Models\\');}
if (!defined('API_PREFIX')) {define('API_PREFIX', 'api');}

/* --- System active */
if (!defined('ACTIVE_TRUE')) {define('ACTIVE_TRUE', 1);}
if (!defined('ACTIVE_FALSE')) {define('ACTIVE_FALSE', 0);}

/* --- Error type file --- */
if (!defined('FILE_ERROR_MAX_SIZE')) {define('FILE_ERROR_MAX_SIZE', 1);}
if (!defined('FILE_ERROR_EXTENSION')) {define('FILE_ERROR_EXTENSION', 2);}
if (!defined('FILE_ERROR_EMPTY')) {define('FILE_ERROR_EMPTY', 3);}

return [
    'System'=>
    [
        'active'=> [
            ACTIVE_TRUE => trans('True'),
            ACTIVE_FALSE => trans('False')
        ],
        'uploadTmp' => 'temp',
        'file_error' => [
            FILE_ERROR_EMPTY => trans('This file is require'),
            FILE_ERROR_MAX_SIZE => trans('File size exceeds allowable'),
            FILE_ERROR_EXTENSION => trans('This file is not valid')
        ]
    ],
];
?>