var domain = document.domain=='localhost'?'grooo-web.local':document.domain;
var protocol = location.protocol;

export const constant: any= {
    BASE_WEB: protocol+'//'+domain+'/',
    BASE_API: protocol+'//'+domain+'/api/',
    BASE_FILE: protocol+'//'+domain+'/files/',

    Active: {
        1: 'Yes',
        0: 'No'
    }
}
