import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import $ from 'jquery'

@Component({
  selector: 'ele-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  public menus;
  private activeMenu;
  constructor(private router: Router) { }

  ngOnInit() {
    this.generateMenus();
    this.activeMenu = window.location.pathname;
  }

  gotoUrl(url) {
    if (!url || url === '#') {
      return false;
    }
    this.activeMenu = url;
    this.checkActiveMenu(this.activeMenu);
    this.router.navigateByUrl(url);
  }

  ngDoCheck() {
    if (this.activeMenu != window.location.pathname) {
      this.activeMenu = window.location.pathname;
      this.checkActiveMenu(this.activeMenu);
    }
  }

  checkActiveMenu(action) {
    let parentActive= $('a[action="'+action+'"]').parents('.menuList');
    if (parentActive.hasClass('open')) {
      return false;
    }
    $('.menuList').removeClass('open');
    $('ul', '.menuList').hide();
    parentActive.addClass('open').find('ul').slideDown();
  }

  generateMenus() {
    this.menus = [
      {
        text: 'Dashboard',
        icon: 'fa-home',
        url: '/dashboard'
      },
      {
        text: 'User',
        icon: 'fa-user',
        url: '#',
        children: [
          {
            text: 'List users',
            url: '/user/list'
          },
          {
            text: 'Add new user',
            url: '/user/form'
          },
        ]
      },
      {
        text: 'Test Menu',
        icon: 'fa-car',
        url: '#',
        children: [
          {
            text: 'List users',
            url: '/test'
          },
          {
            text: 'Add new user',
            url: '/test/form'
          },
        ]
      }
    ];
  }

}
