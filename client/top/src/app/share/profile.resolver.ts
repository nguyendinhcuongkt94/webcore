import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { AppService } from "./app.service";

@Injectable()
export class ProfileResolver implements Resolve<any>
{
    constructor(private app: AppService) {}

    resolve()
    {
        // Dev: Add this if frontend need member login
        //return this.app.get('members/profile');
    }
}
