import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslationPipe } from "./translation.pipe";
import { FooterComponent } from "./element/footer/footer.component";
import { HeaderComponent } from "./element/header/header.component";
import { LanguageSelectorComponent } from "./element/language-selector/language-selector.component";
import { PaginatorComponent } from "./element/paginator/paginator.component";
import { MenuComponent } from "./element/menu/menu.component";
import { UserInfoComponent } from './element/member/info.component';
import { UserLogoutComponent } from './element/member/logout.component';
import { ListPipe } from "./list.pipe";
import { FlashComponent} from "./element/flash/flash.component";
import { BreadcrumbComponent } from './element/breadcrumb/breadcrumb.component';
import { ProfileMenuComponent } from "./element/member/profile-menu.component";
import { RouterModule } from "@angular/router";

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations:
    [
        TranslationPipe,
        ListPipe,
        FooterComponent,
        HeaderComponent,
        LanguageSelectorComponent,
        PaginatorComponent,
        MenuComponent,
        UserInfoComponent,
        UserLogoutComponent,
        FlashComponent,
        BreadcrumbComponent,
        ProfileMenuComponent
    ],
    exports :
    [
        TranslationPipe,
        ListPipe,
        FooterComponent,
        HeaderComponent,
        LanguageSelectorComponent,
        PaginatorComponent,
        MenuComponent,
        UserInfoComponent,
        UserLogoutComponent,
        FlashComponent,
        BreadcrumbComponent
    ]
})

export class ShareModule { }
