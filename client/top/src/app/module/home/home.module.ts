import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { ListComponent } from './list/list.component';
import { ShareModule} from "../../share/share.module";

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        ShareModule
    ],
    declarations: [ListComponent]
})
export class HomeModule { }
