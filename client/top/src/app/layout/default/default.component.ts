import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from "@angular/router";
import { AppService} from "../../share/app.service";

@Component({
    selector: 'app-layout-default',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.css']
})
export class DefaultLayoutComponent implements OnInit {

    constructor(private app: AppService, private route: ActivatedRoute) { }

    ngOnInit() {

        // Dev: Add this if frontend need member login
        /*this.route.data.subscribe((res:any) => {
            this.app.curMember = res.profile.data;
        })*/
    }

    ngAfterViewInit()
    {
        // Load something here
    }
}
