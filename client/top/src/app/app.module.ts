import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { DefaultLayoutComponent } from "./layout/default/default.component";
import { ShareModule } from "./share/share.module";
import { AppService } from "./share/app.service";
import { ProfileResolver } from "./share/profile.resolver";
import { MemberLayoutComponent } from "./layout/member/member.component";

@NgModule({
    declarations: [
        AppComponent,
        DefaultLayoutComponent,
        MemberLayoutComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ShareModule
    ],
    providers: [AppService,ProfileResolver],
    bootstrap: [AppComponent]
})
export class AppModule { }
