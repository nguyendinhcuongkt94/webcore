import { NgModule } from '@angular/core';
import { Routes, RouterModule  } from '@angular/router';
import { DefaultLayoutComponent } from "./layout/default/default.component";
import { MemberLayoutComponent} from "./layout/member/member.component";
import { ProfileResolver} from "./share/profile.resolver";

const routes: Routes =
[
    {
        path: '',
        component: DefaultLayoutComponent,
        children:
        [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './module/home/home.module#HomeModule' }
        ],
        resolve: { profile: ProfileResolver }
    },
    {
        path: 'member',
        component: MemberLayoutComponent,
        children:
        [
            // Dev: Use this layout if member page is different from the homepage layout.
        ]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
