import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from "./layout/default/default.component";
import { LoginLayoutComponent } from "./layout/auth/auth.component";
import { ProfileResolver } from "./share/profile.resolver";

const routes: Routes =
[
    {
        path: '',
        component: DefaultLayoutComponent,
        children:
        [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: './module/dashboard/dashboard.module#DashboardModule' },
            { path: 'user', loadChildren: './module/user/user.module#UserModule' },
            { path: 'categories', loadChildren: './module/category/category.module#CategoryModule' },
            { path: 'news', loadChildren: './module/news/news.module#NewsModule' }
        ],
        resolve: { profile: ProfileResolver }
    },
    {
        path: 'auth',
        component: LoginLayoutComponent,
        children:
        [
            { path: '', loadChildren: './module/auth/auth.module#AuthModule' }
        ]
    },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
