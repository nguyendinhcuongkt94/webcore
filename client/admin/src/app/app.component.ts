import { Component } from '@angular/core';
import '../assets/js/app.seed.js';
declare var initApp: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

    ngAfterViewInit()
    {
        // This need only load one time
        initApp.SmartActions();
    }
}


