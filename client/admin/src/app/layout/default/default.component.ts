import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import $ from "jquery";
import {AppService} from "../../share/app.service";
import '../../../assets/js/app.seed.js';
declare var initApp: any;

@Component({
    selector: 'app-layout-default',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.css']
})
export class DefaultLayoutComponent implements OnInit {

    constructor(private app: AppService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.data.subscribe((res:any) => {
            this.app.curUser = res.profile.data;
        })
    }

    ngAfterViewInit()
    {
        // This need to load every time the layout loaded
        initApp.leftNav();
        initApp.domReadyMisc();
    }

    ngDoCheck() {
        if (!$('#jarviswidget-fullscreen-mode').length) {
          $('body').removeClass('nooverflow');
        }
    }
}
