import { Component, OnInit } from '@angular/core';
import { AppService } from "../../../share/app.service";
import { ActivatedRoute, Router} from "@angular/router";
import { FormData } from "../../../share/form-data";
import { FormArray } from '@angular/forms';

@Component({
    selector: 'app-user-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

    public fd;
    private data = {
        id: '',
        username:'',
        password:'',
        email:'',
        name:'',
        group: null,
        active: 1,
    };

    constructor(
        public app: AppService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.fd = new FormData(this.data);

        if(this.route.snapshot.params['id'])
        {
            this.fd.isNew = false;
            this.app.get('users/form', {'id':this.route.snapshot.params['id']}).subscribe((res:any) => {
                this.fd.setData(res.data);
            });
        }
    }

    save() {
        this.app.post('users/form', this.fd.form.value).subscribe((data:any) =>
        {
            this.app.flashSuccess('User has been saved');

            if(this.app.curUser.id == this.fd.form.get('id').value && this.fd.form.get('active').value == 0) {
                this.app.delConfig('AUTH_TOKEN');
                this.router.navigate(['/auth']);
            }
            else {
                this.router.navigate(['/user']);
            }
        });
    }

    // get tels(): FormArray {
    //     return this.fd.get('tels') as FormArray;
    //   }
      
    //   addTel() {
    //     this.tels.push(this.fd.control(''));
    //   }
      
    //   removeTel(index: number) {
    //     this.tels.removeAt(index);
    //   }

}
