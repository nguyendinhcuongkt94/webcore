import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { AppService } from "../../../share/app.service";
import { FormData } from "../../../share/form-data";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    constructor(private app: AppService) { }

    public fd;
    private data = {
        email:'',
        name:'',
    };

    ngOnInit() {
        this.fd = new FormData(this.data);

        this.app.get('users/profile').subscribe((res:any) => {
            this.fd.setData(res.data);
        });
    }

    save() {
        this.app.post('users/profile', this.fd.form.value).subscribe((data:any) =>
        {
            this.app.flashSuccess('Profile has been updated',true);
        });
    }

}
