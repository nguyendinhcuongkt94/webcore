import { Component, OnInit } from '@angular/core';
import { AppService } from "../../../share/app.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormData } from "../../../share/form-data";
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public fd;
  private data = {
    id: '',
    name: '',
    description: '',
  };
  constructor(
    public app: AppService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  save() {
    this.app.post('categories/form', this.fd.form.value).subscribe((data: any) => {
      this.app.flashSuccess('Category has been saved');
        this.router.navigate(['/categories']);
    });
  }

  ngOnInit() {
    this.fd = new FormData(this.data);

    if (this.route.snapshot.params['id']) {
      this.fd.isNew = false;
      this.app.get('categories/form', { 'id': this.route.snapshot.params['id'] }).subscribe((res: any) => {
        this.fd.setData(res.data);
      });
    }
  }

}
