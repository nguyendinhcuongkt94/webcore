import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from "../category/list/list.component";
import { FormComponent } from "../category/form/form.component";
const routes: Routes = [
  {
    path: '',
    children:
    [
        { path: '', redirectTo:'list' },
        { path: 'list', component: ListComponent },
        { path: 'form', component: FormComponent},
        { path: 'form/:id', component: FormComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
