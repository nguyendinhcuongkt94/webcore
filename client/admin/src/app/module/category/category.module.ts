import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { ListComponent } from '../category/list/list.component';
import { FormComponent } from '../category/form/form.component';
import { ShareModule } from "../../share/share.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    CategoryRoutingModule,
    ShareModule,
    ReactiveFormsModule
  ],
  declarations: [ListComponent, FormComponent]
})
export class CategoryModule { }
