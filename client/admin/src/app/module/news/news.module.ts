import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { ListComponent } from '../news/list/list.component';
import { FormComponent } from '../news/form/form.component';
import { ShareModule } from "../../share/share.module";
import { ReactiveFormsModule } from "@angular/forms";
import { DetailComponent } from './detail/detail.component';
@NgModule({
  imports: [
    CommonModule,
    NewsRoutingModule,
    ShareModule,
    ReactiveFormsModule
  ],
  declarations: [ListComponent, FormComponent, DetailComponent]
})
export class NewsModule { }
