import { Component, OnInit } from '@angular/core';
import { AppService } from "../../../share/app.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormData } from "../../../share/form-data";
import { ListData }  from "../../../share/list-data";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public cd;
  public fd;
  public imgUpload:File;
  public imageSrc: string;
  public data = {
    img:'',
    user_id:'',
  };
  constructor(
    public app: AppService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  save() {
    if(this.imgUpload){
      this.fd.form.value.img = this.imgUpload;
    }
    this.fd.form.value.user_id = this.data.user_id;
    this.app.post('news/form', this.fd.form.value).subscribe((data: any) => {
      this.app.flashSuccess('News has been saved');
        this.router.navigate(['/news']);
    });
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      this.imgUpload = event.srcElement.files[0];
      reader.onload = (event: any) => {
        this.imageSrc = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
}

  ngOnInit() {
    this.cd = new ListData(this.app,this.route,'categories');
    this.fd = new FormData(this.data);
    this.app.get('users/profile').subscribe((res:any) => {
      this.data.user_id = res.data.id
    });
    if (this.route.snapshot.params['id']) {
      this.fd.isNew = false;
      this.app.get('news/form', { 'id': this.route.snapshot.params['id'] }).subscribe((res: any) => {
        this.fd.setData(res.data);
        this.data.img = res.data.img;
      });
    }
  }

}
