import { Component, OnInit } from '@angular/core';
import { AppService} from "../../../share/app.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormData } from "../../../share/form-data";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  public data = {
    id: '',
    img: '',
    title: '',
    content: '',
    category_id: '',
    user_id: '',
    updated_at: '',
    created_at: '',
    category:{
      id:'',
      name:'',
    },
    user:{
      id:'',
      username:'',
      name:'',
    }
  };

  constructor(
    public app: AppService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    if (this.route.snapshot.params['id']) {
      this.app.get('news/form', { 'id': this.route.snapshot.params['id'] }).subscribe((res: any) => {
        this.data = res.data
      });
    }
  }

}
