import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from "../news/list/list.component";
import { FormComponent } from "../news/form/form.component";
import { DetailComponent } from "../news/detail/detail.component";
const routes: Routes = [
  {
    path: '',
    children:
    [
        { path: '', redirectTo:'list' },
        { path: 'list', component: ListComponent },
        { path: 'detail/:id', component: DetailComponent },
        { path: 'form', component: FormComponent},
        { path: 'form/:id', component: FormComponent},
    ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }
