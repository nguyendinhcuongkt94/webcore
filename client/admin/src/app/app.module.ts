import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultLayoutComponent } from "./layout/default/default.component";
import { LoginLayoutComponent } from "./layout/auth/auth.component";
import { AppService } from "./share/app.service";
import { HttpClientModule } from "@angular/common/http";
import { ShareModule } from "./share/share.module";
import { ProfileResolver } from "./share/profile.resolver";
import { CategoryComponent } from './module/category/category.component';
import { NewsComponent } from './module/news/news.component';

@NgModule({
  declarations: [
    AppComponent,
    DefaultLayoutComponent,
    LoginLayoutComponent,
    CategoryComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ShareModule
  ],
  providers: [AppService,ProfileResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
