var domain = document.domain=='localhost'?'grooo-web.local':document.domain;
var protocol = location.protocol;

export const constant: any= {
    BASE_WEB: protocol+'//'+domain+'/',
    BASE_API: protocol+'//'+domain+'/api/',
    BASE_FILE: protocol+'//'+domain+'/files/',

    // User group
    GROUP_ADMIN : 1,
    GROUP_USER : 2,

    User : {
      group : {
        1: 'Group admin',
        2: 'Group member',
      },
      gender : {
        1: 'Male',
        2: 'Female',
      },
    },

    Active: {
        1: 'Yes',
        0: 'No'
    }
}
