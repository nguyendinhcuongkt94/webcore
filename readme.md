# Grooo web framework

Server part: Lumen 5.6 \
Client part: Angular 6

## Development setup

#### Prerequired

 - docker: https://docs.docker.com/install/ 
 - docker-compose: https://docs.docker.com/compose/install/ 
 - laradock: http://laradock.io/ 

#### Install laradock 


Pull laradock

```
git clone https://github.com/Laradock/laradock.git
```

Copy and setting the .env
```
cd laradock
cp env-example .env
```

Then edit the nginx config

File: `nginx/sites/default.conf`

```
server_name localhost; => server_name grooo-web.local;
root /var/www/public; => root /var/www/server/public;
```


Run services

```
sudo docker-compose up -d nginx mariadb phpmyadmin workspace
sudo docker-compose down
sudo docker-compose up -d nginx mariadb phpmyadmin workspace

```

#### Setting the server part


If using laradock:
```
cd laradock
sudo docker-compose exec workspace bash 
```

Setting up

```
cd server
composer install
php artisan migrate
sudo chmod 777 -R storage
sudo chmod 777 -R public/files

```

#### Setting the client part

Admin page

```
cd client/admin
npm install
```

Top page

```
cd client/top
npm install
```

uQ3&=mnS